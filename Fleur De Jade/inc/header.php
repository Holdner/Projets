<?php
    if (session_status() == PHP_SESSION_NONE){
        session_start();
    }
?>
<!DOCTYPE html>
<html lang=fr>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="css/logo-3.ico">

    <title>Fleur de Jade | Spécialités Asiatiques</title>
    
    <link href="css/bootstrap.css" rel="stylesheet">    	
    <link href="css/menu.css" rel="stylesheet">
 

</head>

<body>



		<h2><img src="css/logo.svg" height="25%" width="80%" style="padding:5px 0;"> </h2>
	
			<nav style="padding:5px 0;">	
					<ul>
						
						<li><a href="../index.html">Page d'accueil</a></li>
						<li><a href="../produits.phtml">Produits </a></li>
						<li><a href="../contact.phtml">Contact</a></li>
						<li><a href="../commande.phtml">Commande</a></li>
						<?php if (isset($_SESSION['auth'])): ?>
                    <li> <a href="logout.php">Se déconnecter</a></li>
                <?php else: ?>
                <li><a href="login.php">Se Connecter</a></li>
                <?php endif; ?>
					</ul>
			</nav>	

                
<div class="container">

    <?php if(isset($_SESSION['flash'])): ?>
        <?php foreach($_SESSION['flash'] as $type => $message ): ?>
            <div class="alert alert-<?= $type; ?>">
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
    <?php endif; ?>


