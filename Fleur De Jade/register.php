<?php
require_once 'inc/functions.php';
if (!empty($_POST)) {

    $errors = array();
    require_once 'inc/db.php';

    if (empty($_POST['username']) || !preg_match('/^[A-Za-z0-9_].+$/', $_POST['username'])) {
        $errors['username'] = "Votre pseudo n'est pas valide (Alphanumérique)";
    } else {
        $req = $pdo->prepare('SELECT id FROM users WHERE username = ?');
        $req->execute([$_POST['username']]);
        $user = $req->fetch();
        if ($user) {
            $errors['username'] = 'Ce pseudo est déjà pris';
        }
    }

        if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $errors['email'] = "Votre email n'est pas valide";
        } else {
            $req = $pdo->prepare('SELECT id FROM users WHERE email = ?');
            $req->execute([$_POST['email']]);
            $user = $req->fetch();
            if ($user){
                $errors['email'] = 'Cet email est déjà utilisé par un autre compte';
            }
        }

        if (empty($_POST['bots']) || $_POST['bots'] == 16) {
                echo '';
            }else{
                $errors['email'] = 'Le numéro de confirmation est incorrect';
            }

            //if (isset($_POST['checkbox'])) {
             //   echo '';
            //}else{
            //    $errors['email'] = 'Vous avez pas accepter les condtions du site';
            //}
			
            if (empty($_POST['password']) || $_POST['password'] != $_POST['password_confirm']) {
                $errors['password'] = "Vous devez rentrer un mot de passe valide";
            }

        if (empty($errors)) {
                    $req = $pdo->prepare("INSERT INTO users SET username = ?,email = ?,password = ?, ville = ?, numero = ?,confirmation_token = ?");
                    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                    $token = str_random(60);
                    //$numero = '+33'.$_POST['numero'];
                    $req->execute([$_POST['username'], $_POST['email'], $password, $_POST['ville'],  $_POST['numero'], $token ]);
                    $user_id = $pdo->lastInsertId();
                    mail($_POST['email'], "Confirmation de votre compte", "Afin de valider votre compte, merci de cliquer sur ce lien\n\nhttp://fleur-de-jade.hol.es/confirm.php?id=$user_id&token=$token");
                    $_SESSION['flash']['success'] = 'Un email de confirmation vous a été envoyé pour valider votre compte';
                    header('Location: login.php');
                    exit();
        }
    }
?>

<?php require 'inc/header.php'; ?>

<h1>S'inscrire</h1>

<?php if(!empty($errors)): ?>
<div class="alert alert-danger">
    <p>Vous n'avez pas rempli le formulaire correctement</p>
    <ul>
        <?php foreach($errors as $error): ?>
            <li><?= $error; ?></li>
        <?php endforeach; ?>
        </ul>
</div>
    <?php endif; ?>

<form action="" method="POST">

    <div class="form-group">
        <label for="">Pseudo</label>
        <input type="text" name="username" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Email</label>
        <input type="text" name="email" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Mot de passe</label>
        <input type="password" name="password" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Confirmation de votre mot de passe</label>
        <input type="password" name="password_confirm" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Ville</label>
        <input type="text" name="ville" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Numéro De Téléphone</label>
        <input type="text" name="numero" class="form-control" />
    </div>

    <div class="form-group">
        <label for="">Combien font deux fois huit ?</label>
        <input type="number" name="bots" class="form-control" />
    </div>
	
	    <div class="form-group">
        <label>
            <input type="checkbox" id="conditions" name="conditions" value="1">J'accepte les conditions qui sont appliqué sur ce site
        </label>
    </div>
	
	    <div class="form-group">
        <label>
            <input type="checkbox" name="remember" value="1"> Je souhaite recevoir les mails de <strong>Fleur de Jade</strong>
        </label>
    </div>
	
    <button type="submit" class="btn btn-primary">M'inscrire</button>

</form>

<?php require_once 'inc/footer.php'; ?>